import com.volkov.classwork.sql.core.CRUDCategory;

import java.sql.*;

public class SQLapp {
    private Statement statement;
    private ResultSet resultSet;


    public static void main(String[] args){
        SQLapp sqLapp = new SQLapp();
        sqLapp.runTheApp();
    }

    private void runTheApp() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your Driver!");
            e.printStackTrace();
        }
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/workshop", "root", "g7i6g7m6");


            CRUDCategory.createCategory(connection);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
