package com.volkov.classwork.sql.bin;


import java.sql.Date;

public class Order {
    private int idOrder;
    private Date date;

    public Order() {

    }

    public Order(int idOrder, Date date) {
        this();
        this.idOrder = idOrder;
        this.date = date;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
