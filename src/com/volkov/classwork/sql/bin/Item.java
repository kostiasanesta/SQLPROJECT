package com.volkov.classwork.sql.bin;


public class Item {
    private int idItem;
    private String nameItem;
    private Category category;

    public Item(){

    }
    public Item(int idItem, String nameItem, Category category){
        this();
        this.idItem=idItem;
        this.nameItem = nameItem;
        this.category = category;
    }

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public String getNameItem() {
        return nameItem;
    }

    public void setNameItem(String nameItem) {
        this.nameItem = nameItem;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
