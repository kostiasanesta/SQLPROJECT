package com.volkov.classwork.sql.core;


import java.sql.*;

public class CRUDOrderItem {
    public static void createCategory(Connection connection) {
        String sql = "INSERT INTO T_ORDER_ITEM(ID_ITEM, ID_ORDER, AMOUNT) VALUES(?,?,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setInt(2, 2);
            statement.setInt(3, 10);
            if (statement.executeUpdate() > 0) {
                System.out.println("new category created");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readCategory(Connection connection) {
        String sql = "SELECT * FROM T_ORDER_ITEM";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Integer idCategory = resultSet.getInt(1);
                Integer name = resultSet.getInt(2);
                Integer status = resultSet.getInt(3);
                System.out.println(idCategory + " " + name + " " + status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void updateCategory(Connection connection) {
        String sql = "UPDATE T_ORDER_ITEM SET NAME=? WHERE ID_ITEM=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2, 1);
            statement.setInt(1, 3);
            if (statement.executeUpdate() > 0) {
                System.out.println("category updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCategory(Connection connection) {
        String sql = "DELETE FROM T_ORDER_ITEM WHERE ID_ITEM=?";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, 2);
            if (statement.executeUpdate() > 0) {
                System.out.println("A user was deleted successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
