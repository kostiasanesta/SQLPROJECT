package com.volkov.classwork.sql.core;


import java.sql.*;

public class CRUDCategory {
    public static void createCategory(Connection connection) {
        String sql = "INSERT INTO T_CATEGORY(id_category, name, status) VALUES(?,?,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setString(2, "pervaya kategoria");
            statement.setBoolean(3, true);
            if (statement.executeUpdate() > 0) {
                System.out.println("new category created");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readCategory(Connection connection) {
        String sql = "SELECT * FROM T_CATEGORY";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Integer idCategory = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Boolean status = resultSet.getBoolean(3);
                System.out.println(idCategory+" "+name+" "+status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void updateCategory(Connection connection){
        String sql = "UPDATE T_CATEGORY SET NAME=? WHERE ID_CATEGORY=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,1);
            statement.setString(1,"categoryNEW");
            if (statement.executeUpdate()>0){
                System.out.println("category updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCategory(Connection connection){
        String sql = "DELETE FROM T_CATEGORY WHERE name=?";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, "categoryNew");
            if (statement.executeUpdate() > 0) {
                System.out.println("A user was deleted successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
