package com.volkov.classwork.sql.core;


import java.sql.*;

public class CRUDItem {

    public static void createCategory(Connection connection) {
        String sql = "INSERT INTO T_ITEM(id_item, name, id_category ) VALUES(?,?,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setString(2, "perviy prodykt");
            statement.setInt(3, 1);
            if (statement.executeUpdate() > 0) {
                System.out.println("new category created");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readCategory(Connection connection) {
        String sql = "SELECT * FROM T_ITEM";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Integer idCategory = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Integer status = resultSet.getInt(3);
                System.out.println(idCategory+" "+name+" "+status);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void updateCategory(Connection connection){
        String sql = "UPDATE T_ITEM SET NAME=? WHERE ID_CATEGORY=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,1);
            statement.setString(1,"TOBAR");
            if (statement.executeUpdate()>0){
                System.out.println("category updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCategory(Connection connection){
        String sql = "DELETE FROM T_ITEM WHERE name=?";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, "TOBAR");
            if (statement.executeUpdate() > 0) {
                System.out.println("A user was deleted successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
