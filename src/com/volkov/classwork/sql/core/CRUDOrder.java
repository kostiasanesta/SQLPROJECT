package com.volkov.classwork.sql.core;


import java.sql.*;

public class CRUDOrder {
    public static void createCategory(Connection connection) {
        String sql = "INSERT INTO T_ORDER(id_order, order_time) VALUES(?,?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            statement.setDate(2, new Date(106));
            if (statement.executeUpdate() > 0) {
                System.out.println("new category created");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void readCategory(Connection connection) {
        String sql = "SELECT * FROM T_ORDER";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                Integer idCategory = resultSet.getInt(1);
                Date name = resultSet.getDate(2);
                System.out.println(idCategory+" "+name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public static void updateCategory(Connection connection){
        String sql = "UPDATE T_ORDER SET ORDER_TIME=? WHERE ID_ORDER=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(2,1);
            statement.setDate(1,new Date(123));
            if (statement.executeUpdate()>0){
                System.out.println("category updated");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteCategory(Connection connection){
        String sql = "DELETE FROM T_ORDER WHERE ID_ORDER=?";

        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1, 1);
            if (statement.executeUpdate() > 0) {
                System.out.println("A user was deleted successfully!");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
